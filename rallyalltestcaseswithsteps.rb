require 'logger'
require 'rally_rest_api'
require 'highline/import'

#vars
test_cases = Hash.new()

my_logger = Logger.new STDOUT

username = ""
password = ""

#get username and password from command line
if ARGV.length == 0
	username = ask("Username? ") { |q| q.echo = true }
	password = ask("Password? ") { |q| q.echo = false }
elsif ARGV.length == 1
	username = ARGV.shift
	password = ask("Password? ") { |q| q.echo = false }
elsif ARGV.length == 2
	puts "probably not the best idea to put your password on the command line, but here goes..."
	username = ARGV.shift
	password = ARGV.shift
else
	puts "please run with: rallytestcasereport.rb [username] [password]"
	exit
end

if username.nil? || password.nil? || username.length < 1 || password.length < 1
	puts "please run with: rallytestcasereport.rb [username] [password]"
	exit
end

if username == "fred@adaptive.jp"
	puts "Hi Fred. Keystroke logger installed. What a unique Credit Card number you have!"
end

#rally = RallyRestAPI.new(:username => username, :password => password,:logger => my_logger)
rally = RallyRestAPI.new(:username => username, :password => password)
puts rally.user.login_name

puts "Querying Test Cases..."
query_result = rally.find_all(:testcase,:fetch=>"true",:page_size=>200,:workpace=>"https://rally1.rallydev.com/slm/webservice/1.30/workspace/4489364736",:project=>"https://rally1.rallydev.com/slm/webservice/1.30/project/4489364820")

#build out data structure of test cases
query_result.each do |testcase|
	test_cases[testcase.ref] = Hash.new();
	test_cases[testcase.ref]['testcase'] = testcase
	test_cases[testcase.ref]['testcasesteps'] = Hash.new()
end

puts "Queried "+test_cases.length.to_s()+" Test Cases"
total_test_cases = test_cases.length

puts "Querying Test Case Steps..."
query_result = rally.find_all(:testcasestep,:fetch=>"true",:pagesize=>200,:workpace=>"https://rally1.rallydev.com/slm/webservice/1.30/workspace/4489364736",:project=>"https://rally1.rallydev.com/slm/webservice/1.30/project/4489364820")

#build out data structure of testcasesteps
testcasestep_count=0
query_result.each do |testcasestep|
	testcasestep_count+=1
	unless testcasestep.test_case.nil?
		if testcasestep.state != 'Fixed' && testcasestep.state != 'Closed'
			test_cases[testcasestep.test_case.ref]['testcasesteps'][testcasestep.ref]=testcasestep
		end
	end
end

puts "Received "+testcasestep_count.to_s()+" Test Case Steps"
total_testcasesteps = testcasestep_count

puts "Generating Report..."

#start the report string
report_string = "<html><head><title>Rally Report</title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/></head><body>\n"

#test case table
test_case_table="<table border='1'>"
test_case_table+="<tr><th>Test Case</th><th>Tags</th><th>Steps</th></tr>"
#loop through test cases
test_cases.each do |testcasekey,testcasevalue|
	unless (testcasevalue['testcase'].tags.to_s().include?("REMOVED") || testcasevalue['testcase'].tags.to_s().include?("FUTURE")) && testcasevalue['testcasesteps'].length == 0	
		test_case_table+="<tr>"
		test_case_table+="<td>"+testcasevalue['testcase'].formatted_i_d.to_s()+": "+testcasevalue['testcase'].name.to_s()+"</td><td>"+testcasevalue['testcase'].tags.to_s()+"</td>"

		if testcasevalue['testcasesteps'].length > 0
			#loop through testcasesteps
			test_case_table+="<td><table width='100%' border='1'>"
			test_case_table+="<tr><th>#</th><th>Input</th><th>Expected Result</th></tr>"
			testcasevalue['testcasesteps'].each do |testcasestepkey,testcasestepvalue|
				test_case_table+="<tr><td>"+testcasestepvalue.step_index.to_s()+"</td><td>"+testcasestepvalue.formatted_i_d.to_s()+": "+testcasestepvalue.input.to_s()+"</td><td>"+testcasestepvalue.expected_result.to_s()+"</td></tr>"
			end
			test_case_table+="</table></td>"
		else
			test_case_table+="<td><table width='100%' border='1'>"
			test_case_table+="<tr><th>Input</th><th>Expected Results</th></tr>"
			test_case_table+="<tr><td>"+testcasevalue['testcase'].validation_input.to_s()+"</td><td>"+testcasevalue['testcase'].validation_expected_result.to_s()+"</td></tr>"
			test_case_table+="</table></td>"
		end
		test_case_table+="</tr>"
	end
end

test_case_table+="</table>\n"

#tack the test case table onto the report
report_string += test_case_table

report_string+="</body></html>"

File.open("testcaseswithsteps.html", 'w') {|f| f.write(report_string) }
