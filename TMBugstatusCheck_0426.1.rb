########################################################################
#This example finds a Test Case by formatted ID (e.g. TC1, TC22) and creates a Test Case Result associated to the returned Test Case.
########################################################################
require 'rally_rest_api'

#class tc_result_set <
#  Struct.new(:testcase_id, :aquos, :digno, :iphone4, :iphone4S)
#end


# 'Login to the Rally App'
   @user_name = "akikochow@gmail.com"
   @password = "akko1125a"
   @base_url = "https://rally1.rallydev.com/slm"
onthelist_data = true
project_end_defect_type_data = "High-Priority Other"

#Spec Functional Cosmetic(UI) High-Priority Other

    #Spec defects
#de_number = ['IT-0207', 'IT-0366', 'IT-0367', 'IT-0395', 'IT-0488', 'IT-0647', 'IT-0662', 'IT-0668', 'IT-0674', 'IT-0678', 'IT-0686', 'IT-0691', 'IT-0694', 'IT-0695', 'IT-0712', 'IT-0718', 'IT-0722', 'IT-0724', 'IT-0749', 'IT-0751', 'IT-0764', 'IT-0774', 'IT-0786', 'IT-0789', 'IT-0804', 'IT-0805', 'IT-0807', 'IT-0809', 'IT-0820', 'IT-0836', 'IT-0843', 'IT-0845', 'IT-0850', 'IT-0855', 'IT-0863', 'IT-0865', 'IT-0866', 'IT-0874', 'IT-0875', 'IT-0876', 'IT-0881', 'IT-0882', 'IT-0893', 'IT-0895', 'IT-0928', 'IT-0929', 'IT-0953', 'IT-0954']


#Functional defects
#de_number = ['IT-0969', 'IT-0977', 'IT-0984', 'IT-0985', 'IT-0986', 'IT-0987', 'IT-0988', 'IT-0990', 'IT-0991', 'IT-0414', 'IT-0459', 'IT-0587', 'IT-0608', 'IT-0670', 'IT-0374', 'IT-0389', 'IT-0391', 'IT-0406', 'IT-0407', 'IT-0408', 'IT-0489', 'IT-0625', 'IT-0657', 'IT-0666', 'IT-0671', 'IT-0677', 'IT-0684', 'IT-0689', 'IT-0690', 'IT-0701', 'IT-0702', 'IT-0705', 'IT-0715', 'IT-0720', 'IT-0723', 'IT-0729', 'IT-0730', 'IT-0731', 'IT-0735', 'IT-0757', 'IT-0762', 'IT-0770', 'IT-0798', 'IT-0799', 'IT-0857', 'IT-0862', 'IT-0873', 'IT-0877', 'IT-0892', 'IT-0904', 'IT-0907', 'IT-0916', 'IT-0919', 'IT-0920', 'IT-0923', 'IT-0924', 'IT-0926', 'IT-0927', 'IT-0932', 'IT-0936', 'IT-0937', 'IT-0945', 'IT-0946', 'IT-0947', 'IT-0948', 'IT-0951', 'IT-0957', 'IT-0958', 'IT-0967', 'IT-0887']

#Cosmetic defects
#de_number = ['IT-0917', 'IT-0161', 'IT-0637', 'IT-0698', 'IT-0703', 'IT-0710', 'IT-0883', 'IT-0956', 'IT-0404', 'IT-0450', 'IT-0645', 'IT-0679', 'IT-0704', 'IT-0706', 'IT-0803', 'IT-0826', 'IT-0844', 'IT-0860', 'IT-0867', 'IT-0885', 'IT-0889', 'IT-0933', 'IT-0950', 'IT-0817', 'IT-0935', 'IT-0782', 'IT-0952', 'IT-0151', 'IT-0699', 'IT-0896', 'IT-0217', 'IT-0611', 'IT-0648', 'IT-0707', 'IT-0709', 'IT-0711', 'IT-0713', 'IT-0721', 'IT-0728', 'IT-0742', 'IT-0753', 'IT-0771', 'IT-0783', 'IT-0880', 'IT-0888', 'IT-0925', 'IT-0934', 'IT-0940', 'IT-0942', 'IT-0486', 'IT-0708', 'IT-0714', 'IT-0761', 'IT-0788', 'IT-0833', 'IT-0834', 'IT-0878', 'IT-0890', 'IT-0982']

#MUSTFIX
#de_number = ['IT-0707','IT-0708','IT-0709','IT-0713','IT-0875','IT-0883','IT-0896','IT-0956','IT-0982','IT-1003']

#Yellow
#de_number = ['IT-0365','IT-0583','IT-0687','IT-0688','IT-0694','IT-0728','IT-0766','IT-0767','IT-0771','IT-0786','IT-0790','IT-0793','IT-0800','IT-0801','IT-0806','IT-0816','IT-0821','IT-0840','IT-0858','IT-0878','IT-0884','IT-0895','IT-0929']

#NG
#de_number = ['IT-0377','IT-0475','IT-0505','IT-0506','IT-0530','IT-0552','IT-0555','IT-0561','IT-0678','IT-0680','IT-0702','IT-0720','IT-0792','IT-0842','IT-0861','IT-0893','IT-0898','IT-0929']

#AllRemainingIssues(5/29)
#de_number = ['IT-1077','IT-1086','IT-1093','IT-1098','IT-1096','IT-1107','IT-1106','IT-1108','IT-0393','IT-0561','IT-0612','IT-0792']
#'IT-0490','IT-0505' DUPES of IT-0393
de_number = ['IT-1108','IT-1114','IT-1117','IT-1119','IT-1120','IT-1122','IT-1125']


rally = RallyRestAPI.new(:base_url => @base_url, :username => @user_name, :password => @password)   
puts "Loggin in as "+@user_name+" ..."

x = 0
# Look through and put test results   
de_number.each do |d|
query_result = rally.find(:defect) {equal :t_m_defect_number, d}  
defect_report = query_result.results.first

to_print=''

if defect_report!=nil then
#to_print = d+ ", " + defect_report.priority.to_s() + defect_report.state + ", "+ defect_report.formatted_i_d + ", "+ to_print+defect_report.resolution+", "+defect_report.priority+", On The List: "+defect_report.on_the_list.to_s() + " "+ defect_report.project_end_defect_type.to_s()

#to_print = d+ " "+defect_report.priority.to_s()

to_print = d+ " "+defect_report.state


#rally.update(defect_report,
#:on_the_list => onthelist_data,
#:project_end_defect_type => project_end_defect_type_data
#)

else
to_print = d
end

puts to_print

x=x+1
  
end

