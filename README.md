# Rally TestCase Exporter

Set of tools to extract out Test Cases from Rally for help with UAT.

## Why do I need this tool?

Rally does not allow exporting out of TestCases (or the test case steps).

There are currently two options:

1. Using a Script to export the report (such as the following)
2. Using the [Rally Excel Connector](https://help.rallydev.com/rally-add-excel) (Requires Windows and Excel 2010/2013)

## Requirements

This script uses:

* Ruby - Please see the following for how to [Install Ruby](https://www.ruby-lang.org/en/installation/)

		Please note: all Macintoshes include Ruby by default

* Rally's Ruby Extension [https://rubygems.org/gems/rally_rest_api]

		gem install rally_rest_api

## To Run

[ TODO ] - current reports are AS-IS from Toyota Friend and should be cleaned / streamlined.

## TODO

* Determine difference between [ruby_rest_api](https://rubygems.org/gems/rally_rest_api) and [rally_api](https://github.com/RallyTools/RallyRestToolkitForRuby)

* Clear with Tom if anything is sensitive for non Toyota Friend members

* Describe how to run reports as they are now

* Streamline the list of files / make it clearer which file should be used for what.

* Simplify/Streamline the Setup process to allow more people to use

* Streamline / harden authentication

* Allow for more customization