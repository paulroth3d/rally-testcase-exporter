require 'logger'
require 'rally_rest_api'
require 'highline/import'

#vars
full_report = false

test_cases = Hash.new()
passed_test_cases=0
test_cases_with_no_open_defects=0
test_cases_with_no_open_p1p2_defects=0
test_cases_with_no_open_p1p2p3_defects=0
open_defects=0
fixed_defects=0
closed_defects=0
open_p1p2_defects=0
open_p1p2p3_defects=0
total_test_cases=0
total_defects=0
#regression_start_date = Date.parse("2012-03-21") #sprint 9
#regression_start_date = Date.parse("2012-05-03") #sprint 10
regression_start_date = Date.parse("2012-05-14") #sprint 10
test_cases_run_since_regression_start=0
test_cases_run_today=0
test_cases_for_this_regression=0
test_cases_for_this_regression_run_since_regression_start=0
new_test_cases_for_sprint=0
new_test_cases_for_sprint_run_since_regression_start=0
p1_p2_defects_for_current_sprint_test_cases=0
defects_without_a_test_case = Hash.new()

my_logger = Logger.new STDOUT

username = ""
password = ""

#get username and password from command line
if ARGV.length == 0
	username = ask("Username? ") { |q| q.echo = true }
	password = ask("Password? ") { |q| q.echo = false }
elsif ARGV.length == 1
	username = ARGV.shift
	password = ask("Password? ") { |q| q.echo = false }
elsif ARGV.length == 2
	puts "probably not the best idea to put your password on the command line, but here goes..."
	username = ARGV.shift
	password = ARGV.shift
else
	puts "please run with: rallytestcasereport.rb [username] [password]"
	exit
end

if username.nil? || password.nil? || username.length < 1 || password.length < 1
	puts "please run with: rallytestcasereport.rb [username] [password]"
	exit
end

if username == "fred@adaptive.jp"
	puts "Hi Fred. Keystroke logger installed. What a unique Credit Card number you have!"
end

#rally = RallyRestAPI.new(:username => username, :password => password,:logger => my_logger)
rally = RallyRestAPI.new(:username => username, :password => password)
puts rally.user.login_name

puts "Querying Test Cases..."
query_result = rally.find_all(:testcase,:fetch=>"true",:page_size=>200,:workpace=>"https://rally1.rallydev.com/slm/webservice/1.30/workspace/4489364736",:project=>"https://rally1.rallydev.com/slm/webservice/1.30/project/4489364820")

#build out data structure of test cases
query_result.each do |testcase|
	test_cases[testcase.ref] = Hash.new();
	test_cases[testcase.ref]['testcase'] = testcase
	test_cases[testcase.ref]['defects'] = Hash.new()
	test_cases[testcase.ref]['openp1p2'] = 0
	test_cases[testcase.ref]['openp1p2p3'] = 0
end

puts "Queried "+test_cases.length.to_s()+" Test Cases"
#total_test_cases = test_cases.length

puts "Querying Defects..."
query_result = rally.find_all(:defect,:fetch=>"true",:pagesize=>200,:workpace=>"https://rally1.rallydev.com/slm/webservice/1.30/workspace/4489364736",:project=>"https://rally1.rallydev.com/slm/webservice/1.30/project/4489364820")

#build out data structure of defects
defect_count=0
query_result.each do |defect|
	defect_count+=1
	if defect.state == 'Fixed'
		fixed_defects += 1
	end
	if defect.state == 'Closed'
		closed_defects += 1
	end

	unless defect.test_case.nil?
		if defect.state != 'Fixed' && defect.state != 'Closed' && defect.state != 'Hold'
			open_defects+=1
			test_cases[defect.test_case.ref]['defects'][defect.ref]=defect
			if defect.priority == 'HOTFIX' || defect.priority == 'S -- Resolve Immediately'
				test_cases[defect.test_case.ref]['openp1p2']+=1
				test_cases[defect.test_case.ref]['openp1p2p3']+=1
				open_p1p2_defects+=1
				open_p1p2p3_defects+=1
				if test_cases[defect.test_case.ref]['testcase'].tags.to_s().include?('Sprint10')
					p1_p2_defects_for_current_sprint_test_cases += 1
				end
			elsif defect.priority == 'A -- High Attention'
				test_cases[defect.test_case.ref]['openp1p2p3']+=1
				open_p1p2p3_defects+=1
			end
		end
	else
		if defect.state != 'Fixed' && defect.state != 'Closed' && defect.state != 'Hold'
			defects_without_a_test_case[defect.ref] = defect;
			if defect.priority == 'HOTFIX' || defect.priority == 'S -- Resolve Immediately'
				open_p1p2_defects+=1
				open_p1p2p3_defects+=1				
			elsif defect.priority == 'A -- High Attention'
				open_p1p2p3_defects+=1
			end
		end
	end
end

puts "Received "+defect_count.to_s()+" Defects"
total_defects = defect_count

puts "Generating Report..."

#start the report string
report_string = "<html><head><title>Rally Report</title></head><body>\n"

#test case table
test_case_table="<table border='1'>"
if full_report
	test_case_table+="<tr><th>Test Case</th><th>Tags</th><th>Priority</th><th>Last Run</th><th>Last Verdict</th><th>Open Defects?</th><th>Open P1/P2</th><th>Open P1/P2/P3</th><th>Defects</th></tr>"
else
	test_case_table+="<tr><th>Test Case</th><th>Tags</th><th>Priority</th><th>Last Run</th><th>Open P1/P2</th><th>Defects</th></tr>"
end
#loop through test cases
test_cases.each do |testcasekey,testcasevalue|
	unless testcasevalue['testcase'].tags.to_s().include?("REMOVED") && testcasevalue['defects'].length == 0
		total_test_cases+=1
		test_case_table+="<tr>"
		if full_report
			test_case_table+="<td>"+testcasevalue['testcase'].formatted_i_d.to_s()+": "+testcasevalue['testcase'].name.to_s()+"</td><td>"+testcasevalue['testcase'].tags.to_s()+"</td><td>"+testcasevalue['testcase'].priority.to_s()+"</td><td>"+testcasevalue['testcase'].last_run.to_s()+"</td>"
		else
			test_case_table+="<td>"+testcasevalue['testcase'].formatted_i_d.to_s()+": "+testcasevalue['testcase'].name.to_s()+"</td><td>"+testcasevalue['testcase'].tags.to_s()+"</td><td>"+testcasevalue['testcase'].priority.to_s()+"</td><td>"+testcasevalue['testcase'].last_run.to_s()+"</td>"
		end

		#Test Cases for this regression
		if testcasevalue['testcase'].priority == 'Critical' #|| testcasevalue['testcase'].priority == 'Important'
			test_cases_for_this_regression += 1
		end

		#new test cases for this sprint
		if testcasevalue['testcase'].tags.to_s().include?('Sprint10')
			new_test_cases_for_sprint += 1
		end

		#Test Case Run Since Regression Start Date
		if testcasevalue['testcase'].last_run.to_s().length > 0
			if (Date.parse(testcasevalue['testcase'].last_run.to_s()) <=> regression_start_date) >= 0
				test_cases_run_since_regression_start += 1
				if testcasevalue['testcase'].priority == 'Critical' #|| testcasevalue['testcase'].priority == 'Important'
					test_cases_for_this_regression_run_since_regression_start += 1
				end
				if testcasevalue['testcase'].tags.to_s().include?('Sprint10')
					new_test_cases_for_sprint_run_since_regression_start += 1
				end
			end
			if (Date.parse(testcasevalue['testcase'].last_run.to_s()) <=> Date.today) == 0
				test_cases_run_today += 1
			end
		end

		#Test Case Verdict
		if full_report
			if testcasevalue['testcase'].last_verdict == "Pass"
				passed_test_cases+=1
				test_case_table+="<td bgcolor='#00FF00'>"+testcasevalue['testcase'].last_verdict.to_s()+"</td>\n"
			else
				test_case_table+="<td>"+testcasevalue['testcase'].last_verdict.to_s()+"</td>\n"
			end

			#open defects?
			if testcasevalue['defects'].length < 1
				test_cases_with_no_open_defects+=1
				test_case_table+="<td bgcolor='#00FF00'>NONE</td>\n"
			else
				test_case_table+="<td>YES</td>\n"
			end	
		end
		#open P1 or P2?
		if testcasevalue['openp1p2'] < 1
			test_cases_with_no_open_p1p2_defects+=1
			test_case_table+="<td bgcolor='#00FF00'>NONE</td>\n"
		else
			test_case_table+="<td>YES</td>\n"
		end
		if full_report
			#Open P1 P2 or P3?
			if testcasevalue['openp1p2p3'] < 1
				test_cases_with_no_open_p1p2p3_defects+=1
				test_case_table+="<td bgcolor='#00FF00'>NONE</td>\n"
			else
				test_case_table+="<td>YES</td>\n"
			end
		end
		if testcasevalue['defects'].length > 0
			#loop through defects
			test_case_table+="<td><table width='100%' border='1'>"
			testcasevalue['defects'].each do |defectkey,defectvalue|
				if defectvalue.state != 'Fixed' && defectvalue.state != 'Closed'
					test_case_table+="<tr><td>"+defectvalue.formatted_i_d.to_s()+": "+defectvalue.name+"</td><td>"+defectvalue.priority.to_s()+"</td><td>"+defectvalue.state.to_s()+"</td></tr>"
				end
			end
			test_case_table+="</table></td>"
		else
			test_case_table+="<td></td>"
		end
		test_case_table+="</tr>"
	end
end

test_case_table+="</table>\n"

defects_without_test_case_table="<table width='100%' border='1'>"
defects_without_test_case_table+="<tr><th>ID</th><th>Name</th><th>Priority</th><th>Status</th></tr>"
defects_without_a_test_case.each do |defectkey,defectvalue|
	defects_without_test_case_table+="<tr><td>"+defectvalue.formatted_i_d.to_s()+"</td><td>"+defectvalue.name.to_s()+"</td><td>"+defectvalue.priority.to_s()+"</td><td>"+defectvalue.state.to_s()+"</td></tr>"
end
defects_without_test_case_table+="</table>"

#summary
percentage_test_cases_with_no_p1p2_defects = test_cases_with_no_open_p1p2_defects.to_f()/total_test_cases.to_f()*100.0

report_string += "<p><b>Test Case Report</b></p>"
report_string += "<ul>"
report_string += "<li>Total Test Cases: "+total_test_cases.to_s()+"</li>"
report_string += "<li>Total Test Cases Run Since Regression Start: "+test_cases_run_since_regression_start.to_s()+"</li>"

report_string += "<li>Total Test Cases Targeted For This Regression: "+test_cases_for_this_regression.to_s()+"</li>"
report_string += "<li>Total Test Cases Targeted For This Regression Run Since Regression Start: "+test_cases_for_this_regression_run_since_regression_start.to_s()+"</li>"
report_string += "<li>Targeted Test Case Coverage Since Regression Start: <b>"+sprintf('%.2f',test_cases_for_this_regression_run_since_regression_start.to_f()/test_cases_for_this_regression.to_f()*100)+"%</b></li>"

report_string += "<li>Current Sprint Test Cases: "+new_test_cases_for_sprint.to_s()+"</li>"
report_string += "<li>Current Sprint Test Cases Run Since Regression Start: "+new_test_cases_for_sprint_run_since_regression_start.to_s()+"</li>"
report_string += "<li>Current Sprint Test Coverage Since Regression Start: <b>"+sprintf('%.2f',new_test_cases_for_sprint_run_since_regression_start.to_f()/new_test_cases_for_sprint.to_f()*100)+"%</b></li>"

report_string += "<li>Total Test Cases Run Today: "+test_cases_run_today.to_s()+"</li>"
if full_report
	report_string += "<li>Total Passed Test Cases: "+passed_test_cases.to_s()+"</li>"
	report_string += "<li>Total Test Cases With No Open Defects: "+test_cases_with_no_open_defects.to_s()+"</li>"
end
report_string += "<li>Total PASSING Test Cases (No Open P1 or P2 Defects): "+test_cases_with_no_open_p1p2_defects.to_s()+"</li>"
if full_report
	report_string += "<li>Total Test Cases With No Open P1, P2, or P3 Defects: "+test_cases_with_no_open_p1p2p3_defects.to_s()+"</li>"
end
report_string += "<li>Percentage PASSING Test Cases (No P1 or P2 defects): <b>"+sprintf('%.2f',percentage_test_cases_with_no_p1p2_defects)+"%</b></li>"

report_string += "</ul><p><b>Defect Report</b></p><ul>"
report_string += "<li>Total Defects: "+total_defects.to_s()+"</li>"
report_string += "<li>Total Fixed Defects: "+fixed_defects.to_s()+"</li>"
report_string += "<li>Total Closed Defects: "+closed_defects.to_s()+"</li>"
report_string += "<li>Total Open Defects: "+open_defects.to_s()+"</li>"
report_string += "<li>Total Open P1 or P2 Defects: "+open_p1p2_defects.to_s()+"</li>"
report_string += "<li>Total Open P1, P2, or P3 Defects: "+open_p1p2p3_defects.to_s()+"</li>"
report_string += "<li>P1 or P2 Defects for current sprint test cases: "+p1_p2_defects_for_current_sprint_test_cases.to_s()+"</li>"

report_string += "</ul>"

#tack the test case table onto the report
report_string += test_case_table
report_string += defects_without_test_case_table

report_string+="</body></html>"

File.open("report.html", 'w') {|f| f.write(report_string) }
